class Student{
    constructor(name, email= "", community){
    this.name = name;
    this.email = email;
    this.community = community;
    }
  }
  
  class Bootcamp{
    constructor(name, level, students = []){
      this.name = name;
      this.level= level;
      this.students = students;
      
    }
  registerStudent(student) {        
     
       if(this.students.filter(s => s.email === student.email).length) {
          console.log(`The student ${student.email} is already registered!`);
      } else {
          this.students.push(student);
          console.log(`Registering ${student.email} to the bootcamp ${this.name}.`);
      } 
      return this.students;
  }
  }
  const student3 = new Student("Sasha","email1@gmail.com", "South");
const student2 = new Student ("Olivia", "2email@gmail", "North")
  const student1 = new Student("Sasha","email1", "South");
  
  const bootcamp1 = new Bootcamp("NuCamp", 1);
  
  
   console.log(bootcamp1.students)
  
  
  bootcamp1.registerStudent(student1);
  bootcamp1.registerStudent(student1);
  bootcamp1.registerStudent(student2);
   console.log(bootcamp1.students)
  