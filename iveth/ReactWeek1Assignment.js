class Student {
    constructor (name, email, community) {
        this.name = name;
        this.email = email;
        this.community = Community;
    }
}

class Bootcamp {
    constructor(name, level, students = []) {
        this.name = name;
        this.level = level;
        this.students = students;
    }
    registerStudent(studentToRegister) {
        //if (this.students.find(s => s.email === studentToRegister.email)) {
        if (this.students.filter(s => s.email === studentToRegister.email).length) {
            console.log(`The student ${studentToRegister.email} is already registered!`);
        } else {
            this.students.push(studentToRegister);
            console.log(`Registering ${studentToRegister.email} to the bootcamp ${this.name}.`);
        } 
        return this.students;
    }
}