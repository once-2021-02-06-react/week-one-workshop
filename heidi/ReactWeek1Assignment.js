class Student {
    constructor(name, email, community) {
        this.name = name;
        this.email = email;
        this.community = community;
    }
}

class Bootcamp {
    constructor(name, level, students = []) {
        this.name = name;
        this.level = level;
        this.students = students;
    }
    registerStudent(studentToRegister) {
        if (this.students.find(s => s.email === studentToRegister.email)) {
            console.log(`The student ${studentToRegister.email} is already registered!`)
        } else {
            this.students.push(studentToRegister);
            console.log(`Registering ${studentToRegister.email} to the bootcamp ${this.name}`)
        }
        return this.students;

    }
}

//Testing

/*
const webDevFund = new Bootcamp('Web Dev Fundamentals', 'Beginner');
const fullStack = new Bootcamp('Full Stack Web Development', 'Advanced');

const heidi = new Student('Heidi', 'heidi@nucamp.com', 'Round Rock');
const yvette = new Student('Yvette', 'yvette@nucamp.com', 'Austin');
const karen = new Student('Karen', 'karen@nucamp.org', 'Round Rock');

webDevFund.registerStudent(heidi);                                      
webDevFund.registerStudent(yvette);
fullStack.registerStudent(karen);
fullStack.registerStudent(karen); //student should be prev registered

*/