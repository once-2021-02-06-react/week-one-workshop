class Student { 
    constructor(name,email,community) {
        this.name = name;
        this.email = email;
        this.community = community;
    }
};

class Bootcamp { 
    constructor(name,level,students=[]) {
        this.name = name;
        this.level = level;
        this.students = students;
    }
    registerStudent(studentToRegister) {
        if(this.students.filter(registering=>registering.email===studentToRegister.email).length){
            console.log("This email is already registered")
        }
        else {
            this.students.push(studentToRegister);
            console.log(`successfully registering ${studentToRegister.email} to the bootcamp Web Dev fundamentals!`)
        }
        
    }
}